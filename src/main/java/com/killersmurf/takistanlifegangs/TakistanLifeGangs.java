package com.killersmurf.takistanlifegangs;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author holmd834
 */
public class TakistanLifeGangs extends JavaPlugin {

    private static YamlConfiguration dataFile;
    private static TakistanLifeGangs instance;
    public static final Logger LOGGER = Logger.getLogger("Minecraft");

    @Override
    public void onEnable() {
        instance = this;
    }

    @Override
    public void onDisable() {
    }

    /**
     * Gets the instance of the data file.
     *
     * @return a YamlConfiguration file.
     */
    public static YamlConfiguration getDataFile() {
        return dataFile;
    }
    
    public static TakistanLifeGangs getInstance() {
        return instance;
    }

    /**
     * loads the data file.
     *
     * @return a YamlConfiguration file with the data from /data.yml
     */
    public YamlConfiguration loadDataFile() {
        File df = new File("./data.yml");

        if (!df.exists()) {
            try {
                df.createNewFile();
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, "Could not create the data file!", ex);
            }
        }
        dataFile = YamlConfiguration.loadConfiguration(df);
        return dataFile;
    }
    
    public static void saveDataFile() {
        File df = new File("./data.yml");
        if (!df.exists()) {
            df.mkdir();
        }
        try {
            dataFile.save(df);
        } catch (IOException ex) {
            Logger.getLogger(TakistanLifeGangs.class.getName()).log(Level.SEVERE, "Cannot save data file!", ex);
        }
    }
}