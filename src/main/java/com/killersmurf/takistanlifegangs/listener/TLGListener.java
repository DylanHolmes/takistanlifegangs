/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.takistanlifegangs.listener;

import com.killersmurf.takistanlifegangs.GangUtil;
import com.killersmurf.takistanlifegangs.gangs.Gang;
import com.killersmurf.takistanlifegangs.gangs.GangMember;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.getspout.spoutapi.Spout;

/**
 *
 * @author holmd834
 */
public class TLGListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (!event.getPlayer().hasPlayedBefore()) {
            GangMember member = new GangMember(Spout.getServer().getPlayer(event.getPlayer().getName()));
            GangUtil.addPlayer(member);
            return;
        }
        GangMember member;
        member = GangUtil.getMember(event.getPlayer().getName());
        if (member == null) {
            member = new GangMember(Spout.getServer().getPlayer(event.getPlayer().getName()));
            GangUtil.addPlayer(member);
        }
        Gang gang = member.getGang();
        if (gang.getMember(member.getName()) == null) {
            gang.addMember(member);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerMove(PlayerMoveEvent event) {
        Location from = event.getFrom();
        Location to = event.getTo();
        if (from.distanceSquared(to) <= 1) {
            return;
        } 
        if (!GangUtil.getGang(from.getChunk()).getName().
                equals(GangUtil.getGang(to.getChunk()).getName())) {
            Gang gang = GangUtil.getGang(to.getChunk());
            event.getPlayer().sendMessage(ChatColor.AQUA + gang.getName() + "'s territory.");
        }
    }
}
