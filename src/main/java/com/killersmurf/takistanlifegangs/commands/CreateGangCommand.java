/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.takistanlifegangs.commands;

import com.dylanholmes.takistanlife.economy.Bank;
import com.dylanholmes.takistanlife.economy.BankAccount;
import com.killersmurf.takistanlifecore.util.TakistanCommand;
import com.killersmurf.takistanlifedivisions.division.Division;
import com.killersmurf.takistanlifedivisions.player.TLDPlayer;
import com.killersmurf.takistanlifedivisions.util.PlayerUtil;
import com.killersmurf.takistanlifegangs.GangUtil;
import com.killersmurf.takistanlifegangs.gangs.Gang;
import com.killersmurf.takistanlifegangs.gangs.GangMember;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 *
 * @author holmd834
 */
public class CreateGangCommand extends TakistanCommand {

    @Override
    public void execute(CommandSender cs, Command cmnd, String string, String[] strings) {
        if (!(cs instanceof SpoutPlayer)) {
            cs.sendMessage("The console cannot own a gang.");
            return;
        }
        SpoutPlayer sPlayer = (SpoutPlayer) cs;
        TLDPlayer dPlayer = PlayerUtil.getPlayer(sPlayer.getName());
        if (!dPlayer.getDivision().equals(Division.CIVILIAN)) {
            sPlayer.sendMessage(ChatColor.RED + "You must be a Civilian to access this command.");
            return;
        }
        if (Bank.getAccount(sPlayer.getName()).getMoney() < 50000) {
            sPlayer.sendMessage(ChatColor.RED + "You do not have enough money to create a Gang.");
            return;
        }
        BankAccount account = Bank.getAccount(sPlayer.getName());
        
        if (dPlayer.isRestrained() || dPlayer.isStunned() || dPlayer.isJailed()) {
            sPlayer.sendMessage(ChatColor.RED + "You cannot create a gang while stunned, restrained, or in jail.");
            return;
        }
        
        GangMember member = GangUtil.getMember(sPlayer);
        if (member.getGang() != null) {
            sPlayer.sendMessage(ChatColor.RED + "You are already in a gang.");
            return;
        }
        
        if (strings.length < 1) {
            sPlayer.sendMessage(ChatColor.RED + "You must enter a Gang name. /creategang <Gang Name>");
        }
        
        String name = strings[0]; 
        Gang gang = new Gang(name);
        account.subtractMoney(50000);
    }
}
