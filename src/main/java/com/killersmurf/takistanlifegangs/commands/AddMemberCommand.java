/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.takistanlifegangs.commands;

import com.killersmurf.takistanlifecore.util.TakistanCommand;
import com.killersmurf.takistanlifedivisions.division.Division;
import com.killersmurf.takistanlifedivisions.player.TLDPlayer;
import com.killersmurf.takistanlifedivisions.util.PlayerUtil;
import com.killersmurf.takistanlifegangs.GangUtil;
import com.killersmurf.takistanlifegangs.TakistanLifeGangs;
import com.killersmurf.takistanlifegangs.gangs.Gang;
import com.killersmurf.takistanlifegangs.gangs.GangMember;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.getspout.spoutapi.player.SpoutPlayer;
import org.spout.api.Spout;

/**
 *
 * @author holmd834
 */
public class AddMemberCommand extends TakistanCommand {

    @Override
    public void execute(CommandSender cs, Command cmnd, String string, String[] strings) {
        if (!(cs instanceof SpoutPlayer)) {
            cs.sendMessage("The console cannot own a gang.");
            return;
        }
        SpoutPlayer sPlayer = (SpoutPlayer) cs;
        TLDPlayer dPlayer = PlayerUtil.getPlayer(sPlayer.getName());
        if (!dPlayer.getDivision().equals(Division.CIVILIAN)) {
            sPlayer.sendMessage(ChatColor.RED + "You must be a Civilian to access this command.");
            return;
        }
        GangMember member = GangUtil.getMember(sPlayer);
        Gang gang = member.getGang();
        if (strings.length < 1) {
            sPlayer.sendMessage(ChatColor.RED + "You must enter a player's name. /addmember <Player's Name>");
            return;
        }
        if (member.getGang() != null && member.getPrivileges() >= 1) {
            final GangMember receiver = GangUtil.getMember(strings[0]);
            if (receiver.getGang() != null) {
                sPlayer.sendMessage(ChatColor.RED + "This player is already in a gang, he must leave first before he can join yours.");
                return;
            }
            receiver.setGang(gang);
            receiver.setInvited(true);
            Bukkit.getScheduler().scheduleSyncDelayedTask(TakistanLifeGangs.getInstance(), new Runnable() {

                public void run() {
                    receiver.setGang(null);
                    receiver.setInvited(false);
                }
            }, 1200L);
        }
    }
}
