/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.takistanlifegangs;

import com.killersmurf.takistanlifegangs.gangs.Gang;
import com.killersmurf.takistanlifegangs.gangs.GangMember;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.getspout.spoutapi.Spout;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 *
 * @author holmd834
 */
public class GangUtil {

    public static Map<String, Gang> gangs = new HashMap<String, Gang>();
    public static Map<String, GangMember> players = new HashMap<String, GangMember>();
    public static List<Entry<Integer, Integer>> gangAreas = new ArrayList<Entry<Integer, Integer>>();

    public GangUtil() {
        YamlConfiguration df = TakistanLifeGangs.getDataFile();
        gangAreas = (List<Entry<Integer, Integer>>) df.getList("server.gang.areas");
    }

    public static Gang getGang(String gang) {
        return gangs.get(gang);
    }

    public static void addGang(Gang gang) {
        if (gangs.containsKey(gang.getName())) {
            return;
        }
        gangs.put(gang.getName(), gang);
    }

    public static boolean isChunkClaimed(Chunk chunk) {
        for (Gang gang : gangs.values()) {
            if (!gang.getChunks().contains(chunk)) {
                continue;
            }
            return true;
        }
        return false;
    }

    public static Gang getGang(Chunk chunk) {
        for (Gang gang : gangs.values()) {
            if (!gang.getChunks().contains(chunk)) {
                continue;
            }
            return gang;
        }
        return null;
    }

    public static boolean isChunkClaimed(Block block) {
        return isChunkClaimed(block.getChunk());
    }

    public static boolean isChunkClaimed(World world, int x, int z) {
        if (world == null) {
            world = Bukkit.getWorld("world");
        }
        return isChunkClaimed(world.getChunkAt(x, z));
    }

    /**
     *
     *
     * @param name
     * @return
     */
    public static GangMember getMember(String name) {
        return players.get(name);
    }

    /**
     * A method that gets a GangMember from a Spout Player
     *
     * @param player
     * @return
     */
    public static GangMember getMember(SpoutPlayer player) {
        return getMember(player.getName());
    }

    /**
     * A method that adds a new GangMember into the map.
     *
     * @param name
     * @param bool
     */
    public static void addPlayer(String name, boolean bool) {
        if (players.containsKey(name)) {
            return;
        }
        GangMember player = new GangMember(Spout.getServer().getPlayer(name));
        player.load();
        players.put(name, player);
    }

    /**
     * A method that adds an existing GangMember into the map.
     *
     * @param player
     */
    public static void addPlayer(GangMember player) {
        if (players.containsKey(player.getName())) {
            return;
        }
        players.put(player.getName(), player);
    }

    public void save() {
    }
}