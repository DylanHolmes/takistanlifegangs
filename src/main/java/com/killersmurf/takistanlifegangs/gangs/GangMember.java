package com.killersmurf.takistanlifegangs.gangs;

import com.killersmurf.takistanlifegangs.GangUtil;
import com.killersmurf.takistanlifegangs.TakistanLifeGangs;
import org.bukkit.configuration.file.YamlConfiguration;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 *
 * @author holmd834
 */
public class GangMember {

    private String name;
    private Gang gang;
    private SpoutPlayer player;
    public static Integer privileges;
    private boolean invited;

    public GangMember(SpoutPlayer player) {
        this.player = player;
        this.name = player.getName();
        load();
    }

    public String getName() {
        return name;
    }

    public boolean isInvited() {
        return invited;
    }

    public void setInvited(boolean invited) {
        this.invited = invited;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SpoutPlayer getPlayer() {
        return player;
    }

    public Integer getPrivileges() {
        if (privileges == null) {
            return -1;
        }
        return privileges;
    }

    public void setPrivileges(int privileges) {
        GangMember.privileges = privileges;
    }

    public Gang getGang() {
        return gang;
    }

    public void setGang(Gang gang) {
        this.gang = gang;
    }

    public final void load() {
        YamlConfiguration df = TakistanLifeGangs.getDataFile();
        setGang(GangUtil.getGang(df.getString("players." + name + ".gang")));
        setPrivileges(df.getInt("players." + name + ".privileges"));
    }

    public void save() {
        YamlConfiguration df = TakistanLifeGangs.getDataFile();
        df.set("players." + name + ".privileges", privileges);
        df.set("players." + name + ".gang", getGang().getName());
        //RESERVED FOR POTENTIAL NEW VARIABLES

        df.set("players." + name + ".", null);
        df.set("players." + name + ".", null);
    }
}