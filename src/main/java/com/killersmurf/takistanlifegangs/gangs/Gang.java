/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.takistanlifegangs.gangs;

import com.killersmurf.takistanlifegangs.GangUtil;
import com.killersmurf.takistanlifegangs.TakistanLifeGangs;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.getspout.spoutapi.Spout;

/**
 *
 * @author holmd834
 */
public class Gang {

    private String name;
    private Double income;
    private Map<String, GangMember> members = new HashMap<String, GangMember>();
    private List<Chunk> land = new ArrayList<Chunk>();

    public Gang(String name) {
        this.name = name;
        load(null);
    }

    public Gang(String name, GangMember owner) {
        this.name = name;
        load(owner);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String addChunk(Chunk chunk) {
        if (GangUtil.isChunkClaimed(chunk)) {
            return "";
        }
        land.add(chunk);
        return "";
    }

    public Double getIncome() {
        return 0.0;
    }

    public void addChunk(Block block) {
        land.add(block.getChunk());
    }

    public List<Chunk> getChunks() {
        return land;
    }

    public Chunk getChunk(Block block) {
        if (!land.contains(block.getChunk())) {
            return null;
        }
        return block.getChunk();
    }

    public List<GangMember> getMembers() {
        return (List) members.values();
    }

    public GangMember getOwner() {
        for (GangMember member : getMembers()) {
            if (member.getPrivileges() == 3) {
                return member;
            }
        }
        return null;
    }

    public void addMember(GangMember member) {
        members.put(member.getName(), member);
    }

    public void addMember(String name) {
        GangMember member = new GangMember(Spout.getServer().getPlayer(name));
        addMember(member);
    }

    public GangMember getMember(String name) {
        if (!members.containsKey(name)) {
            return null;
        }
        return members.get(name);
    }

    public void save() {
        YamlConfiguration df = TakistanLifeGangs.getDataFile();
        List<Entry<Integer, Integer>> chunkPairs = new ArrayList<Entry<Integer, Integer>>();
        for (Chunk chunk : land) {
            chunkPairs.add(new SimpleEntry<Integer, Integer>(chunk.getX(), chunk.getZ()));
        }
        df.set("gangs." + name + ".land", chunkPairs);
        df.set("gangs." + name + ".income", income);
        df.set("gangs." + name + ".members", (List) members.keySet());


    }

    public final void load(GangMember owner) {
        YamlConfiguration df = TakistanLifeGangs.getDataFile();
        List<Entry<Integer, Integer>> chunkPairs = (List<Entry<Integer, Integer>>) df.getList("gangs." + name + ".land", null);
        if (chunkPairs != null) {
            for (Entry<Integer, Integer> entry : chunkPairs) {
                Chunk chunk = Bukkit.getWorld("world").getChunkAt(entry.getKey(), entry.getValue());
                land.add(chunk);
            }
        }

        income = df.getDouble("gangs." + name + ".income", 0);
        List<String> memberList = df.getStringList("gangs." + name + ".members");
        if (memberList != null) {
            for (Iterator<String> it = memberList.iterator(); it.hasNext();) {
                String memName = it.next();
                members.put(memName, null);
            }
        } else {
            if (owner != null) {
                members.put(owner.getName(), owner);
            }
        }
        GangUtil.addGang(this);
    }
}